import logo from './logo.svg';
import './App.css';
import React, { useState, useEffect  } from 'react';


function App() {

  const [signals, setSignals] = useState([]);

  const fetchSignals = () => {
    fetch('http://127.0.0.1:5000/signals/calculate')
    .then(response => response.json())
    .then(data => setSignals(data.records) )
    .catch(error => console.log(error)); 
  };

  const tableRecords = signals.map(r => {
    return <tr>
    <td>{r.recomendation}</td>
    <td>{r.symbol}</td>
    <td>{r.timestamp}</td>
  </tr>
  });

  return (
    <div className="App">
      <button onClick={fetchSignals} >Calculate</button>
      <table>
        <tbody>
        {tableRecords}
        </tbody>
      </table>
    </div>
  );
}

export default App;
