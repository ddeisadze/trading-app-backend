import pandas as pd
import numpy as np
import yfinance
import pandas_datareader as pdr
import requests_cache
import json
from pandas_datareader.yahoo.headers import DEFAULT_HEADERS
import datetime
from flask import jsonify
from flask import Flask
from flask_cors import CORS


app = Flask(__name__)
CORS(app)

def signals_to_json():
    signals = calculate_signals()
    positions_buy = signals["positions"] > 0
    positions_sell = signals["positions"] < 0
    positions = signals[positions_sell | positions_buy ]
    positions3 = positions.to_dict(orient="index")
    list_dict = []
    for i in positions3:
        dict = {}
    #     print(((i.replace(tzinfo=None).to_pydatetime())- datetime.datetime(1970,1,1)).total_seconds())
        df = i.replace(tzinfo=None).to_pydatetime()
    #     print((datetime.datetime(i)- datetime.datetime(1970,1,1)).datetime.total_seconds())
    #     print(type(i))
        epoch_time = (df - datetime.datetime(1970,1,1)).total_seconds()
    #     print(epoch_time)
        dict["timestamp"] = int(epoch_time)
        dict["symbol"] = "BTC-USD"
        if positions3[i]['positions'] == 1:
            dict["recomendation"] = "BUY"
        else:
            dict["recomendation"] = "SELL"
        list_dict.append(dict)
    # json_list_dict = json.dumps(list_dict)
    return {"records":list_dict}



def calculate_signals():
    session = requests_cache.CachedSession(cache_name='cache', backend='sqlite')
    session.headers = DEFAULT_HEADERS
    # btc_data = pdr.get_data_yahoo(['BTC-USD'], 
    #                           start=datetime.datetime(2021, 1, 1), 
    #                           end=datetime.datetime(2021, 10, 22),
    #                              interval="min")

    btc_data = yfinance.download(tickers="BTC-USD",start=datetime.datetime(2021, 1, 1),
                                end=datetime.datetime(2021, 10, 22),interval="1h", session=session)



    a = yfinance.download(tickers="BTC-USD",period="5d",interval="1m")

    # Initialize the short and long windows
    short_window = 40
    long_window = 100

    # Initialize the `signals` DataFrame with the `signal` column
    signals = pd.DataFrame(index=btc_data.index)
    signals['signal'] = 0.0

    # Create short simple moving average over the short window
    signals['short_mavg'] = btc_data['Close'].rolling(window=short_window, min_periods=1, center=False).mean()

    # Create long simple moving average over the long window
    signals['long_mavg'] = btc_data['Close'].rolling(window=long_window, min_periods=1, center=False).mean()

    # Create signals
    signals['signal'][short_window:] = np.where(signals['short_mavg'][short_window:] 
                                                > signals['long_mavg'][short_window:], 1.0, 0.0)   

    # Generate trading orders
    signals['positions'] = signals['signal'].diff()

    # Print `signals`
    return signals

@app.route("/signals/calculate")
def users_api():
    return jsonify(signals_to_json())

if __name__ == '__main__':
  app.run(debug=True)